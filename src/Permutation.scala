
class Permutation {

  //выявляем центральные 4 квадрата
  def Generate(squares: Array[Square]) {
    for (i <- squares) //i левый верхний квадрат
      for (j <- squares) //j правый верхний квадрат
        if (i != j)
          for (x <- squares) //x - левый нижний квадрат
            if (x != j && x != i)
              for (y <- squares) //y - правый нижний квадрат
                if (y != x && y != j && y != i)
                  if (i.rd + j.ld + x.ru + y.lu == 10)
                    if ((i.ru + j.lu <= 10) && (i.ld + x.lu <= 10) && (x.rd + y.ld <= 10) && (j.rd + y.ru <= 10)) {
                      //записываем 4 центральных квадрата удовлетворяющих условиям
                      val sq4 = new Array[Square](12)
                      sq4(3) = i
                      sq4(4) = j
                      sq4(7) = x
                      sq4(8) = y
                      //получаем список неиспользованных квадратов
                      val unused: List[Square] = squares.filter(elem => ((elem != x) && (elem != y) && (elem != i) && (elem != j))).toList
                      addLeft(sq4, unused)
                    }


  }

  //добавляем 2 квадрата слева(2 и 6)
  def addLeft(currentSQ: Array[Square], unused: List[Square]): Unit = {
    for (i <- unused)
      for (j <- unused.filter(j => (j!=i)))
      //if (i != j)
        if (i.rd + j.ru + currentSQ(3).ld + currentSQ(7).lu == 10)
          if (i.ld + j.lu <= 10) {
            currentSQ(2) = i
            currentSQ(6) = j
            //убираем из листа неиспользованных квадратов 2 квадрата
            val unused1: List[Square] = unused.filter(item => (item != i && item != j))

            addUp(currentSQ, unused1)
          }
  }

  //добавляем 2 квадрата сверху(0 и 1)
  def addUp(currentSQ: Array[Square], unused: List[Square]): Unit = {
    for (i <- unused)
      for (j <- unused.filter(j => (j!=i)))
        if (i.rd + j.ld + currentSQ(3).ru + currentSQ(4).lu == 10)
          if (i.ru + j.lu <= 10) {
            currentSQ(0) = i
            currentSQ(1) = j
            //убираем из листа неиспользованных квадратов 2 квадрата
            val unused1: List[Square] = unused.filter(item => (item != i && item != j))

            addRight(currentSQ, unused1)
          }
  }

  //добавляем 2 квадрата справа(5 и 9)
  def addRight(currentSQ: Array[Square], unused: List[Square]): Unit = {
    for (i <- unused)
      for (j <- unused.filter(j => (j!=i)))
        if (i.ld + j.lu + currentSQ(4).rd + currentSQ(8).ru == 10)
          if (i.rd + j.ru <= 10) {
            currentSQ(5) = i
            currentSQ(9) = j
            //убираем из листа неиспользованных квадратов 2 квадрата
            val unused1: List[Square] = unused.filter(item => (item != i && item != j))

            addDown(currentSQ, unused1)
          }
  }

  //добавляем 2 квадрата снизу(10 и 11)
  def addDown(currentSQ: Array[Square], unused: List[Square]): Unit = {
    for (i <- unused)
      for (j <- unused.filter(j => (j!=i)))
        if (i.ru + j.lu + currentSQ(7).rd + currentSQ(8).ld == 10)
          if (i.rd + j.ld <= 10) {
            currentSQ(10) = i
            currentSQ(11) = j
            printResult(currentSQ)
          }
  }

  //проверка на "трехугловые <10" и вывод результата
  def printResult(resultSQ: Array[Square]): Unit = {
    if (resultSQ(2).ru + resultSQ(3).lu + resultSQ(0).ld <= 10)
      if (resultSQ(1).rd + resultSQ(4).ru + resultSQ(5).lu <= 10)
        if (resultSQ(6).rd + resultSQ(7).ld + resultSQ(10).lu <= 10)
          if (resultSQ(9).ld + resultSQ(8).rd + resultSQ(11).ru <= 10)
          {
            for (i <- resultSQ)
              println(i.lu + " " + i.ru + " " + i.ld + " " + i.rd)
            println()
          }
  }

}
