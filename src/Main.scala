import java.io.FileInputStream
import java.util.Scanner

object Main {

  def main(args: Array[String]): Unit = {
    //Reading from file
    val fileName: String = System.getProperty("user.dir") + "\\Input.txt"
    try {
      val col = new Scanner(new FileInputStream(fileName))

      val squares = new Array[Square](12)
      for (i <- 0 until 12) {
        val lu = col.nextInt()
        val ru = col.nextInt()
        val ld = col.nextInt()
        val rd = col.nextInt()
        squares(i) = Square(lu, ru, ld, rd)
      }
      val perm: Permutation = new Permutation
      perm.Generate(squares)
    }
    catch {
      case e: Exception => println("reading error")
    }

  }

}
